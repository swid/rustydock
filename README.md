Rustydock
=========

A Ubuntu 14.04 based rust development image for docker.

Featuring
=========
  - cargo via rustup.sh
  - bash as CMD
  - forced color prompt in bash
  - $USER set in .bashrc (for cargo)

Usage
=====
By default a bash for the user 'user' is spawned.

```docker run -ti swid/rustydock```
